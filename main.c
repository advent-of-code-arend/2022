#include "includes/utils.h"

void usage(int exit_code) {
    fprintf(stderr, "Usage: %s -d <project_dir>\n", getprogname());
    exit(exit_code);
}

int main(int argc, char** argv) {
    int c;
    char* project_dir = NULL;
    int day = -1;
    while ((c = getopt(argc, argv, "d:h:s:")) != EOF) {
        switch (c) {
            case 'd':
                project_dir = strdup(optarg);
                break;
            case 'h':
                usage(EXIT_SUCCESS);
            case 's':
                day = atoi(optarg);
                break;
            default:
                usage(EXIT_FAILURE);
        }
    }
    if (NULL == project_dir) {
        project_dir = strdup("/aoc/2022");
    }

    int rc = -1 != day ? solve_day(project_dir, day) : solve_days(project_dir);
    free(project_dir);
    return rc;
}
