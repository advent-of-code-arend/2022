//
// Created by Arend Hummeling on 30/11/2022.
//

#ifndef AOC2022_UTILS_H
#define AOC2022_UTILS_H

#ifndef __APPLE__
#include "macos.h"
#endif

#include "stdio.h"
#include "stdbool.h"
#include "ctype.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include "time.h"
#include "libgen.h"
#include "sys/param.h"
#include "stdarg.h"
#include "signal.h"
#include "assert.h"

#define BUFF_SIZE 1024

#define MAX_SOLUTION_LENGTH 12

typedef struct ListNode ListNode;

typedef struct ListNode {
    void* value;
    ListNode* next;
} ListNode;

typedef struct Stack {
    int height;
    int *members;
    int column;
} Stack;

typedef struct Daily {
    int (*a)(FILE *);
    int (*b)(FILE *);
} Daily;

typedef struct Range {
    int lower_bound;
    int upper_bound;
} Range;

typedef struct Position {
    int x;
    int y;
} Position;

typedef enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
} Direction;

void set_solution(const char *fmt, ...);
int solve_day(char *, int);
int solve_days(char *);

void swap(void **, void **);

bool range_contains_range(Range, Range);
bool r1_contains_r2(Range, Range);
bool r2_contains_r1(Range, Range);
bool range_overlaps_with_range(Range, Range);
int range_avg(Range);

void push(Stack *, int);
int pop(Stack *);
int peek(Stack *);

bool array_contains(const int *, int value, size_t);
bool array_contains_pos(const Position*, Position, size_t);
bool array_unique(const int *array, size_t);
void array_rotate(int *, size_t, int);

int first_unique_string(FILE *, size_t);
void println(int *, size_t, FILE *);
void nextline(FILE *);

void move_nup(Position* p, int i);
void move_ndown(Position* p, int i);
void move_nleft(Position* p, int i);
void move_nright(Position* p, int i);
void move_n(Position* p, Direction d, int i);
void move_up(Position* p);
void move_down(Position* p);
void move_left(Position* p);
void move_right(Position* p);
void move(Position* p, Direction d);

int int_to_normal(int);
Position d_pos(Position, Position);
Position add_pos(Position, Position);
bool eq_pos(Position, Position);
void move_d(Position*, Position);

ListNode* create_node(void* value);
ListNode* append_node(ListNode*, void*);
void prepend_node(ListNode**, ListNode**);
void free_list(ListNode*);

void assert_char(char c, char e);
void fassert_char(FILE* f, char e);
void fassert_word(FILE* f, char* word, size_t len);
void fvassert_word(FILE* f, char* fmt, ...);

int fgetn(FILE* f, char delimiter);
int flookahead(FILE* f);
void fskipif(FILE* f, int c);

int d01a(FILE *);
int d02a(FILE *);
int d03a(FILE *);
int d04a(FILE *);
int d05a(FILE *);
int d06a(FILE *);
int d07a(FILE *);
int d08a(FILE *);
int d09a(FILE *);
int d10a(FILE *);
int d11a(FILE *);
int d12a(FILE *);
int d13a(FILE *);
int d14a(FILE *);
int d15a(FILE *);
int d16a(FILE *);
int d17a(FILE *);
int d18a(FILE *);
int d19a(FILE *);
int d20a(FILE *);
int d21a(FILE *);
int d22a(FILE *);
int d23a(FILE *);
int d24a(FILE *);
int d25a(FILE *);
int d01b(FILE *);
int d02b(FILE *);
int d03b(FILE *);
int d04b(FILE *);
int d05b(FILE *);
int d06b(FILE *);
int d07b(FILE *);
int d08b(FILE *);
int d09b(FILE *);
int d10b(FILE *);
int d11b(FILE *);
int d12b(FILE *);
int d13b(FILE *);
int d14b(FILE *);
int d15b(FILE *);
int d16b(FILE *);
int d17b(FILE *);
int d18b(FILE *);
int d19b(FILE *);
int d20b(FILE *);
int d21b(FILE *);
int d22b(FILE *);
int d23b(FILE *);
int d24b(FILE *);
int d25b(FILE *);


#endif //AOC2022_UTILS_H
