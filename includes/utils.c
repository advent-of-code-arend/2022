//
// Created by Arend Hummeling on 30/11/2022.
//
#include <sys/errno.h>
#include "utils.h"

static char* solution_string = NULL;


static const Daily daily[25] = {
        {d01a, d01b},
        {d02a, d02b},
        {d03a, d03b},
        {d04a, d04b},
        {d05a, d05b},
        {d06a, d06b},
        {d07a, d07b},
        {d08a, d08b},
        {d09a, d09b},
        {d10a, d10b},
        {d11a, d11b},
        {d12a, d12b},
        {d13a, d13b},
        {d14a, d14b},
        {d15a, d15b},
        {d16a, d16b},
        {d17a, d17b},
        {d18a, d18b},
        {d19a, d19b},
        {d20a, d20b},
        {d21a, d21b},
        {d22a, d22b},
        {d23a, d23b},
        {d24a, d24b},
        {d25a, d25b},
};

void free_solution() {
    free(solution_string);
}

void print_timing(clock_t c, const char* prefix) {
    printf("%s", prefix);
    printf("%.0fns\t", (double) clock() - c);
}

void set_solution(const char* fmt, ...) {
    va_list ap;
    if (NULL == solution_string) {
        solution_string = (char*) malloc(BUFF_SIZE);
        atexit(free_solution);
    }
    va_start(ap, fmt);
    vsnprintf(solution_string, MAX_SOLUTION_LENGTH + 1, fmt, ap);
    if (strlen(solution_string) == 0) {
        fprintf(stderr, "Solution is empty string\n");
        exit(EXIT_FAILURE);
    }
    char* buff = strdup(solution_string);
    va_end(ap);
    snprintf(solution_string, MAX_SOLUTION_LENGTH + 1, "%-12s", buff);
    free(buff);
}

char* get_solution() {
    if (NULL == solution_string) {
        fprintf(stderr, "Solution string is not set\n");
        exit(EXIT_FAILURE);
    }

    return strdup(solution_string);
}

void clear_solution() {
    strcpy(solution_string, "Unsolved");
}

FILE* get_file(char* project_dir, int day_number) {
    clock_t start = clock();
    char* dir = malloc(BUFF_SIZE);
    snprintf(dir, BUFF_SIZE, "%s/src/day%02i/%s", project_dir, day_number, NULL != getenv("TEST_EXAMPLE") ? "example.txt" : "input.txt");
    char* buff = strdup(dir);
    free(dir);

    FILE *f = fopen(buff, "r");
    if (NULL == f) {
        fprintf(stderr, "Failed to open file at: %s\n", buff);
        exit(EXIT_FAILURE);
    }
    print_timing(start, "get_file: ");
    free(buff);


    return f;
}

int time_solve(int (*fn)(FILE*), FILE* f, const char* fmt) {
    clock_t start = clock();
    int ret = fn(f);
    printf(fmt, (double) (clock() - start));

    return ret;
}

int solve_days(char* project_dir) {
    clock_t start = clock();
    int sum = 0;
    for (int i = 1; i <= atoi(getenv("TODAY")); i++) {
        sum += solve_day(project_dir, i);
    }
    printf("Total solving time: %.0fns\n", (double) (clock() - start));

    return sum;
}

int solve_day(char* project_dir, int day_number) {
    printf("day: %d\t", day_number);
    solution_string = malloc(BUFF_SIZE);
    strcpy(solution_string, "Unsolved");
    clock_t start = clock();
    FILE* f = get_file(project_dir, day_number);
    Daily d = daily[day_number - 1];
    time_solve(d.a, f, "A: %4.0fns\t");
    char* a = get_solution();
    printf("%s\t", a);
    clear_solution();
    rewind(f);
    free(a);
    time_solve(d.b, f, "B: %4.0fns\t");
    char* b = get_solution();
    printf("%s\t", b);
    clear_solution();
    fclose(f);
    free(b);
    printf("total: %5.0fns\n", (double) (clock() - start));
    free(solution_string);
    fflush(stdout);

    return 0;
}

void swap(void** p1, void** p2) {
    void* tmp;
    tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
}

bool r1_contains_r2(Range r1, Range r2) {
    return r1.lower_bound <= r2.lower_bound && r1.upper_bound >= r2.upper_bound;
}

bool r2_contains_r1(Range r1, Range r2) {
    return r2.lower_bound <= r1.lower_bound && r2.upper_bound >= r1.upper_bound;
}

bool range_contains_range(Range r1, Range r2) {
    return r1_contains_r2(r1, r2) || r2_contains_r1(r1, r2);
}

int range_avg(Range r) {
    return (r.upper_bound + r.lower_bound) / 2;
}

bool number_in_range(Range r, int n) {
    return r.lower_bound <= n && r.upper_bound >= n;
}

bool range_overlaps_with_range(Range r1, Range r2) {
    // TODO: this is ugly and inefficient.
    for (int i = r2.lower_bound; i <= r2.upper_bound; i++) {
        if (number_in_range(r1, i)) {
            return true;
        }
    }
    return false;
}

void nextline(FILE* f) {
    while ('\n' != fgetc(f) && !feof(f));
}

void push(Stack* stack, int member) {
    stack->members[stack->height++] = member;
}

int pop(Stack* stack) {
    return stack->members[--stack->height];
}

int peek(Stack* stack) {
    return stack->members[stack->height - 1];
}

bool array_contains(const int* array, int value, size_t length) {
    for (int i = 0; i < length; i++) {
        if (array[i] == value) {
            return true;
        }
    }

    return false;
}

bool array_unique(const int* array, size_t length) {
    int buff[length];
    buff[0] = array[0];
    for (int i = 1; i < length; i++) {
        if (array_contains(buff, array[i], i)) {
            return false;
        }
        buff[i] = array[i];
    }

    return true;
}

void array_rotate(int* array, size_t length, int val) {
    for (int i = 0; i < length - 1; i++) {
        array[i] = array[i + 1];
    }
    array[length - 1] = val;
}

int first_unique_string(FILE* f, size_t length) {
    int* buff = (int*) malloc(length * sizeof(int));
    for (int i = 0; i < length; i++) {
        buff[i] = fgetc(f);
    }
    while (!feof(f) && !array_unique(buff, length)) {
        array_rotate(buff, length, fgetc(f));
    }
    free(buff);

    return (int) ftell(f);
}

void println(int* line, size_t len, FILE* f) {
    if (NULL == f) {
        f = stdout;
    }
    for (int i = 0; i < len; i++) {
        fputc(line[i], f);
    }
    fputc('\n', f);
}

void move_nup(Position* p, int i) {
    p->y -= i;
}

void move_ndown(Position* p, int i) {
    p->y += i;
}

void move_nleft(Position* p, int i) {
    p->x -= i;
}

void move_nright(Position* p, int i) {
    p->x += i;
}

void move_n(Position* p, Direction d, int i) {
    switch (d) {
        case UP:
            move_nup(p, i);
            break;
        case DOWN:
            move_ndown(p, i);
            break;
        case LEFT:
            move_nleft(p, i);
            break;
        case RIGHT:
            move_nright(p, i);
            break;
    }
}


void move_up(Position* p) {
    p->y--;
}

void move_down(Position* p) {
    p->y++;
}

void move_left(Position* p) {
    p->x--;
}

void move_right(Position* p) {
    p->x++;
}

void move(Position* p, Direction d) {
    switch (d) {
        case UP:
            move_up(p);
            break;
        case DOWN:
            move_down(p);
            break;
        case LEFT:
            move_left(p);
            break;
        case RIGHT:
            move_right(p);
            break;
    }
}

void move_d(Position* p, Position v) {
    p->x += v.x;
    p->y += v.y;
}

int int_to_normal(int i) {
    return i == 0 ? 0 : (i < 0 ? -1 : 1);
}

Position d_pos(Position p1, Position p2) {
    int x = p1.x - p2.x;
    int y = p1.y - p2.y;
    if (abs(x) < 2 && abs(y) < 2) {
        return (Position) {0, 0};
    }

    return (Position) {int_to_normal(x), int_to_normal(y)};
}

Position add_pos(Position p1, Position p2) {
    return (Position) {
        p1.x + p2.x,
        p1.y + p2.y,
    };
}

bool eq_pos(Position p1, Position p2) {
    return p1.x == p2.x && p1.y == p2.y;
}

bool array_contains_pos(const Position* arr, Position p, size_t s) {
    for (int i = 0; i < s; i++) {
        if (eq_pos(arr[i], p)) {
            return true;
        }
    }
    return false;
}

ListNode* create_node(void* value) {
    ListNode* node = (ListNode*) malloc(sizeof(ListNode));
    node->value = value;
    node->next = NULL;

    return node;
}

ListNode* append_node(ListNode* tail, void* value) {
    if (NULL == tail) {
        tail = create_node(value);
        return tail;
    }
    if (tail->next != NULL) {
        fprintf(stderr, "Cannot append node to non-terminating node\n");
        exit(EXIT_FAILURE);
    }

    tail->next = create_node(value);

    return tail->next;
}

void prepend_node(ListNode** new_tail, ListNode** old_tail) {
    (*new_tail)->next = *old_tail;
}

void free_list(ListNode* head) {
    if (NULL == head) {
        fprintf(stderr, "Warning: Freeing empty list\n");
        return;
    }
    free(head->value);
    if (NULL != head->next) {
        free_list(head->next);
    }
    free(head);
}

void assert_char(char c, char e) {
//    fprintf(stderr, "Testing (%d|%c) == (%d|%c)\n", c, c, e, e);
    if (c == e) {
//        fprintf(stderr, "assert %d == %d passed\n", c, e);
        return;
    }

    fprintf(stderr, "assert_char failed, got %d, expected (%d|%c)\n", c, e, e);
    exit(EXIT_FAILURE);
}

void fassert_char(FILE* f, char e) {
    char c = fgetc(f);
    assert_char(c, e);
}

void fassert_word(FILE* f, char* word, size_t len) {
//    fprintf(stderr, "Testing for string: %s\n", word);
    for (int i = 0; i < len - 1; i++) {
        fassert_char(f, word[i]);
    }
}

void fvassert_word(FILE* f, char* fmt, ...) {
    size_t len = strlen(fmt);
    char* buff = (char*) malloc(len * sizeof(char));
    va_list va;
    va_start(va, fmt);
    vsnprintf(buff, len, fmt, va);
    va_end(va);
    fassert_word(f, buff, len);
    free(buff);
}

int fgetn(FILE* f, char delimiter) {
    int c = 'a';
    int i = 0;
    char* buff = (char*) malloc(16 * sizeof(char));
    fpos_t fpos;

    while (c != delimiter && c != EOF && c != '\n') {
        fgetpos(f, &fpos);
        c = fgetc(f);
        if (isnumber(c)) {
            buff[i++] = c;
        }
    }
    if (c == '\n' || c == EOF) {
        fsetpos(f, &fpos);
    }

    int result = atoi(buff);
    free(buff);

    return result;
}

int flookahead(FILE* f) {
    fpos_t offset;
    fgetpos(f, &offset);
    int c = fgetc(f);
    fsetpos(f, &offset);

    return c;
}

void fskipif(FILE* f, int c) {
    fpos_t fpos;
    fgetpos(f, &fpos);
    if (c != fgetc(f)) {
        fsetpos(f, &fpos);
    }
}