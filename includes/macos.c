//
// Created by Arend Hummeling on 12/12/2022.
//

#include "macos.h"

const char* getprogname() {
    return "aoc2022";
}

int isnumber(int c) {
    return c > 47 && c < 58;
}