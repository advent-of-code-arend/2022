//
// Created by Arend Hummeling on 12/12/2022.
//

#ifndef AOC2022_MACOS_H
#define AOC2022_MACOS_H

const char* getprogname();
int isnumber(int);

#endif //AOC2022_MACOS_H
