#!/bin/bash
set -eu

REPO="git://git.sv.gnu.org/autoconf"
BRANCH="v2.71"


if ! command -v cmake >/dev/null; then
  bash /scripts/cmake.sh
fi

function install_autoconf() {
  apt-get -yqq update && apt-get -yqq install help2man
  if [ ! -d /root/autoconf ]; then
    git clone -b "${BRANCH}" --single-branch "${REPO}" /root/autoconf
  fi
  OLD_PWD=$PWD
  cd /root/autoconf
  git submodule update --init
  autoreconf -vi
  ./configure
  make MAKEINFO=/bin/true
  make install MAKEINFO=/bin/true
  cd $OLD_PWD
}

install_autoconf
