#!/bin/bash
set -eu

REPO="https://sourceware.org/git/binutils-gdb.git"
BRANCH="gdb-12-branch"


if ! command -v ninja >/dev/null; then
  bash /scripts/ninja.sh
fi

if ! command -v bison >/dev/null; then
  apt-get -yqq update && apt-get -yqq install bison
fi

if ! command -v flex >/dev/null; then
  apt-get -yqq update && apt-get -yqq install flex
fi

function install_gdb() {
  if [ ! -d /root/gdb ]; then
    git clone -b "${BRANCH}" --single-branch "${REPO}" /root/gdb
  fi
  OLD_PWD=$PWD
  cd /root/gdb
  ./configure
  make MAKEINFO=/bin/true
  make install MAKEINFO=/bin/true
  cd $OLD_PWD
}

if ! command -v gdb >/dev/null; then
  (install_gdb)
fi