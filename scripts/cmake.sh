#!/bin/bash
set -eu

REPO="https://github.com/Kitware/CMake.git"
BRANCH="v3.25.1"


function do_install() {
  git clone --single-branch -b "${BRANCH}" "${REPO}" /root/cmake
  OLD_PWD=$PWD
  cd /root/cmake
  ./bootstrap
  gmake
  gmake install
  cd $OLD_PWD
}

if ! command -v cmake > /dev/null; then
  (do_install)
fi