#!/bin/bash
set -euo pipefail

cmake -DCMAKE_MAKE_PROGRAM=/usr/bin/ninja -DCMAKE_BUILD_TYPE=Debug -B /aoc/2022/cmake-build-debug -S /aoc/2022 -G Ninja
#cmake -B /aoc/2022/cmake-build-release

cmake --build /aoc/2022/cmake-build-debug --target all -j 8
#cmake --build /aoc/2022/cmake-build-release --target all -j 8

#mv /aoc/2022/cmake-build-release/aoc2022 /usr/bin/aoc2022
mv /aoc/2022/cmake-build-debug/aoc2022 /usr/bin/aoc2022

aoc2022 -s 11

