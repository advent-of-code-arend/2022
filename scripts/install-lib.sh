#!/bin/bash

function install_target() {
  TARGET="$1"
  REPO="$2"
  BRANCH="$3"
  if [ -d "/root/${TARGET}" ]; then
    git clone --single-branch -b "${BRANCH}" "${REPO}" "/root/${TARGET}"
    git -C "/root/${TARGET}" submodule update --init
  fi
  OLD_PWD="${PWD}"
  if [ -f bootstrap ]; then
    ./bootstrap
  fi

  if [ $# -gt 3 ]; then
    eval "$4"
  fi

  if [ -f configure ]; then
    ./configure --prefix=/usr/bin
  fi

  if [ -f Makefile ]; then
    make MAKEINFO=/bin/true
    make install MAKEINFO=/bin/true
  fi

  if [ -f CMakeLists.txt ]; then
    cmake -Bbuild-cmake
    cmake --build build-cmake
    mv "build-cmake/${TARGET}" "/usr/bin/${TARGET}"
  fi
  cd "${OLD_PWD}"
}