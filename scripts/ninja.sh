#!/bin/bash
set -eu

REPO="https://github.com/ninja-build/ninja.git"
BRANCH="v1.11.1"


if ! command -v cmake > /dev/null; then
  bash /scripts/install_cmake.sh
fi

function install_ninja() {
  git clone --single-branch -b "${BRANCH}" "${REPO}" /root/ninja
  OLD_PWD=$PWD
  cd /root/ninja
  cmake -Bbuild-cmake
  cmake --build build-cmake
  mv build-cmake/ninja /usr/bin/ninja
  cd $OLD_PWD
}

if ! command -v ninja >/dev/null; then
  (install_ninja)
fi