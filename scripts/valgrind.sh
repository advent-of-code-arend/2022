#!/bin/bash
set -eu

REPO="https://sourceware.org/git/valgrind.git"
BRANCH="VALGRIND_3_20_0"


if ! command -v cmake >/dev/null; then
  bash /scripts/cmake.sh
fi

function install_valgrind() {
  git clone --single-branch -b "${BRANCH}" "${REPO}" /root/valgrind
  OLD_PWD=$PWD
  cd /root/valgrind
  ./autogen.sh
  ./configure --prefix=/
  make
  make install
  valgrind ls -l
  cd $OLD_PWD
}

if ! command -v valgrind >/dev/null; then
  (install_valgrind)
fi