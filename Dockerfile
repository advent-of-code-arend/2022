FROM gcc:12.2.0-bullseye
SHELL ["/bin/bash", "-c"]

RUN mkdir /scripts
COPY ./install-env.sh /usr/bin/install-env

# 893.4s - 958.7s
COPY ./scripts/cmake.sh /scripts/cmake.sh
RUN install-env -e cmake

# 27.5s - 33.4s
COPY ./scripts/ninja.sh /scripts/ninja.sh
RUN install-env -e ninja

# 52.4s
COPY ./scripts/autoconf.sh /scripts/autoconf.sh
RUN install-env -e autoconf

#
COPY ./scripts/gdb.sh /scripts/gdb.sh
RUN install-env -e gdb

# 355.8s - 375.4s
COPY ./scripts/valgrind.sh /scripts/valgrind.sh
RUN install-env -e valgrind

COPY ./scripts/rebuild.sh /usr/bin/rebuild
COPY ./scripts/aoc-valgrind /scripts/aoc-valgrind

RUN mkdir -p /aoc/2022
WORKDIR /aoc/2022

COPY main.c .
COPY CMakeLists.txt .

COPY includes ./includes
COPY src ./src
COPY ./scripts/aoc-valgrind /usr/bin/aoc-valgrind

ENV TODAY 10

CMD ["aoc2022"]