//
// Created by Arend Hummeling.
//

#include "helper.h"



int d08a(FILE* f){
    int** map = get_map(f);
    int** visible = get_map(NULL);
    look_at_trees(map, visible);
    int s = bool_sum_map(visible);
    free_map(map);
    set_solution("%d", s);
    free_map(visible);
    return 0;
}



