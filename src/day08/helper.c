//
// Created by Arend Hummeling on 09/12/2022.
//

#include "helper.h"

int** get_map(FILE* f) {
    int c;
    int x = 0, y = 0;
    int** map = (int**) malloc(MAP_SIZE * sizeof(int*));
    for (int i = 0; i < MAP_SIZE; i++) {
        map[i] = (int*) calloc(MAP_SIZE, sizeof(int));
        for (int j = 0; j < MAP_SIZE; j++) {
            if (map[i][j] != 0) {
                map[i][j] = 0;
            }
        }
    }
    if (NULL == f) {
        return map;
    }
    while ((c = fgetc(f)) != EOF) {
        if (c == '\n') {
            y++;
            x = 0;
            continue;
        }
        map[y][x++] = c - 48;
    }

    return map;
}

void print_map(int** map) {
    putchar('\n');
    for (int i = 0; i < MAP_SIZE; i++) {
        for (int j = 0; j < MAP_SIZE; j++) {
            printf("%d", map[i][j]);
        }
        putchar('\n');
    }
}

void look_at_trees(int** map, int** visible) {
    int previous, curr;
    for (int i = 0; i < MAP_SIZE; i++) {
        previous= -1;
        for (int j = 0; j < MAP_SIZE; j++) {
            curr = map[i][j];
            if (curr <= previous) {
                continue;
            }
            visible[i][j] = 1;
            previous = curr;
        }
    }
    for (int i = 0; i < MAP_SIZE; i++) {
        previous = -1;
        for (int j = MAP_SIZE - 1; j >= 0; j--) {
            curr = map[i][j];
            if (curr <= previous) {
                continue;
            }
            visible[i][j] = 1;
            previous = curr;
        }
    }

    for (int i = 0; i < MAP_SIZE; i++) {
        previous = -1;
        for (int j = 0; j < MAP_SIZE; j++) {
            curr = map[j][i];
            if (curr <= previous) {
                continue;
            }
            visible[j][i] = 1;
            previous = curr;
        }
    }
    for (int i = 0; i < MAP_SIZE; i++) {
        previous = -1;
        for (int j = MAP_SIZE - 1; j >= 0; j--) {
            curr = map[j][i];
            if (curr <= previous) {
                continue;
            }
            visible[j][i] = 1;
            previous = curr;
        }
    }
}

int bool_sum_map(int** map) {
    int s = 0;
    for (int i = 0; i < MAP_SIZE; i++) {
        for (int j = 0; j < MAP_SIZE; j++) {
            s += map[j][i] != 0;
        }
    }
    return s;
}

void free_map(int** map) {
    for (int i = 0; i < MAP_SIZE; i++) {
        free(map[i]);
    }
    free(map);
}