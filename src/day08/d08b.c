//
// Created by Arend Hummeling.
//

#include "helper.h"

int calculate_viewing_distance(int **map, int x, int y) {
    int base_height = map[y][x];
    int dxmin = 0, dxmax = 0, dymin = 0, dymax = 0;
    int x0 = x;
    int y0 = y;
    while (x > 0 && (map[y][x] < base_height || x == x0)) {
        x--;
        dxmin++;
    }
    if (!dxmin) {
        return 0;
    }
    x = x0;
    while (x < MAP_SIZE - 1 && (map[y][x] < base_height || x == x0)) {
        dxmax++;
        x++;
    }
    if (!dxmax) {
        return 0;
    }
    x = x0;
    while (y > 0 && (map[y][x] < base_height || y == y0)) {
        dymin++;
        y--;
    }
    if (!dymin) {
        return 0;
    }
    y = y0;
    while (y < MAP_SIZE - 1 && (map[y][x] < base_height || y == y0)) {
        dymax++;
        y++;
    }
    return dxmin * dxmax * dymin * dymax;
}

int d08bget_solution(int** map) {
    int max = 0, d;
    for (int i = 1; i < MAP_SIZE - 1; i++) {
        for (int j = 1; j < MAP_SIZE -1 ; j++) {
            d = calculate_viewing_distance(map, i, j);
            if (d > max) {
                max = d;
            }
        }
    }

    return max;
}

int d08b(FILE *f) {
    int **map = get_map(f);
    int max = d08bget_solution(map);

    free(map);
    set_solution("%d", max);
    return 0;
}
