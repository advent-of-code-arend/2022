//
// Created by Arend Hummeling on 09/12/2022.
//

#ifndef AOC2022_HELPER_H
#define AOC2022_HELPER_H

#include "../../includes/utils.h"
#define MAP_SIZE 99

int** get_map(FILE*);
int bool_sum_map(int**);
void look_at_trees(int**, int**);
void print_map(int**);
void free_map(int**);
#endif //AOC2022_HELPER_H
