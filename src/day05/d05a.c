//
// Created by Arend Hummeling.
//

#include "../../includes/utils.h"

static const size_t n_stacks = 10;
static const size_t top_height = 8;
static const size_t max_stack_size = 70;
static const size_t n_crates = 53;

typedef struct Move {
    int n;
    int from;
    int to;
} Move;

void execute_move(Stack* crates, Move m, bool keep_order) {
    if (keep_order) {
        int* mbuff = malloc(BUFF_SIZE * sizeof(int));
        if (NULL == mbuff) {
            fprintf(stderr, "Memory allocation for new stack failed\n");
            exit(EXIT_FAILURE);
        }
        Stack s = {m.n, mbuff, -1};
        for (int i = 0; i < m.n; i++) {
            int v = pop(&(crates[m.from]));
            push(&s, v);
        }
        for (int i = 0; i < m.n; i++) {
            int v2 = pop(&s);
            push(&(crates[m.to]), v2);
        }
        free(mbuff);
    } else {
        for (int i = 0; i < m.n; i++) {
            push(&(crates[m.to]), pop(&(crates[m.from])));
        }
    }
}

void print_stack(Stack* stack) {
    int* tmp = malloc(max_stack_size * sizeof(int));
    int h = stack->height;
    while (stack->height > 0 ) {
        int val = pop(stack);
        tmp[stack->height] = val;
        printf("[%c] ", val);
    }
    printf("\n");

    while (h-- > 0) {
        push(stack, tmp[h]);
    }
    free(tmp);
}

void print_lines(int** lines, size_t height) {
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < (n_stacks - 1) * 4; j++) {
            putchar(lines[i][j]);
        }
        putchar('\n');
    }
}

int sh_to_lh(int sh) {
    return (int) (n_crates - 1) - sh;
}

int col_to_offset(int col) {
    return (col - 1) * 4;
}

void print_stacks(Stack* stacks) {
    int** lines = (int**) malloc(n_crates * sizeof (int*));
    for (int i = 0; i < n_crates; i++) {
        lines[i] = (int *) malloc(40 * sizeof(int));
    }

    for (int i = 0; i < n_crates; i++) {
        for (int j = 0; j < 4 * n_stacks; j++) {
            lines[i][j] = ' ';
        }
    }
    int x, y;
    for (int i = 1; i < n_stacks; i++) {
        Stack* stack = &stacks[i];
        for (int h = 0; h < stack->height; h++) {
            y = sh_to_lh(h);
            x = col_to_offset(stack->column);
            lines[y][x] = '[';
            lines[y][x + 1] = stack->members[h];
            lines[y][x + 2] = ']';
            lines[y][x + 3] = ' ';
        }
    }

    print_lines(lines, n_crates);
}

void solve(FILE* f, bool keep_order) {
    Stack* crates = (Stack*) malloc(n_stacks * sizeof(Stack));
    for (int i = 1; i < n_stacks; i++) {
        crates[i] = (Stack) {0, (int*) calloc(max_stack_size, sizeof (int)), i};
    }

    int lineno = (int) top_height, ci;
    int** lines = (int**) malloc(top_height * sizeof (int*));
    size_t* line_lens = calloc(top_height, sizeof(size_t));
    for (int i = 0; i < top_height; i++) {
        int* line = (int *) malloc(40 * sizeof(int));
        int col = 0;
        while ((ci = fgetc(f)) != '\n') {
            line[col++] = ci;
        }
        line[col] = '\0';
        line_lens[i] = col;
        lines[i] = line;
    }

    for (int i = lineno - 1; i >= 0; i--) {
        int* line = lines[i];
        for (int col = 1; col < line_lens[i]; col+=4) {
            int c = line[col];
            int scol = (col  + 3) / 4;
            Stack* stack = &crates[scol];
            if (stack->column != scol) {
                fprintf(stderr, "Found stack with invalid column value: %d at %d\n", stack->column, scol);
            }
            if (isalpha(c)) {
                push(stack, c);
            }
        }
    }
    free(line_lens);

    nextline(f);
    nextline(f);

    const char* move_format = "move %d from %d to %d";
    int move, from, to;

    while (!feof(f)) {
        fscanf(f, move_format, &move, &from, &to);
        nextline(f);
        execute_move(crates, (Move) {move, from, to}, keep_order);
    }


    char* buff = (char*) calloc(n_stacks + 1, sizeof(char));
    for (int i = 1; i < n_stacks; i++) {
        int c = pop(&crates[i]);
        if (isalpha(c)) {
            buff[i - 1] = (char) c;
        }
    }
    buff[n_stacks] = '\0';
    set_solution("%s", buff);

    free(buff);
    for (int i = 0; i < top_height; i++) {
        free(lines[i]);
    }
    for (int i = 1; i < n_stacks; i++) {
        free(crates[i].members);
    }
    free(crates);
    free(lines);
}


int d05a(FILE* f){
    solve(f, false);

	return 0;
}

