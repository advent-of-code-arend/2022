//
// Created by Arend Hummeling on 09/12/2022.
//

#include "helper.h"

static int PXMAX = - INT_MAX;
static int PXMIN = INT_MAX;
static int PYMAX = - INT_MAX;
static int PYMIN = INT_MAX;
static int** map = NULL;

void validate_map() {
    if (NULL == map) {
        fprintf(stderr, "Map is not yet set\n");
        exit(EXIT_FAILURE);
    }
}

bool map_is_set() {
    return NULL != map;
}

int** d09_get_map() {
    validate_map();
    return map;
}

void set_map() {
    if (NULL != map) {
        fprintf(stderr, "Map was already set\n");
        exit(EXIT_FAILURE);
    }
    assert (PYMIN >= 0);
    assert (PXMIN >= 0);
    assert (PYMIN < PYMAX);
    assert (PXMIN < PXMAX);
    size_t ylen = ((PYMAX - PYMIN) + 1) * sizeof(int*);
    size_t xlen = ((PXMAX - PXMIN) + 1) * sizeof(int);
    assert (ylen > 0);
    assert (xlen > 0);

    map = (int**) malloc(ylen);
    for (int i = PYMIN; i <= PYMAX; i++) {
        map[i] = (int*) malloc(xlen);
        for (int j = PXMIN; j <= PXMAX; j++) {
            map[i][j] = 0;
        }
    }
}

void update_boundaries(Position* h) {
    if (map != NULL) {
        fprintf(stderr, "Cannot update positions after map was set\n");
        exit(EXIT_FAILURE);
    }
    if (h->x > PXMAX) {
        PXMAX = h->x;
    }
    if (h->x < PXMIN) {
        PXMIN = h->x;
    }
    if (h->y > PYMAX) {
        PYMAX = h->y;
    }
    if (h->y < PYMIN) {
        PYMIN = PYMIN;
    }
}

Direction d09_parse_direction(char c) {
    switch (c) {
        case 'L':
            return LEFT;
        case 'R':
            return RIGHT;
        case 'U':
            return UP;
        case 'D':
            return DOWN;
    }
    fprintf(stderr, "Unexpected character encountered: (i: %d) at %s:%d\n", c, __FILE__, __LINE__);
    exit(EXIT_FAILURE);
}

void pos_arr_add(PosArr* posArr, Position p) {
    if (posArr->allocated - posArr->size < 2) {
        fprintf(stderr, "posArr->members was allocated with too little memory: %lu\n", posArr->allocated);
        exit(EXIT_FAILURE);
    }
    posArr->members[posArr->size++] = p;
}

void update_rope(ListNode* current, PosArr* posArr) {
    Position* h = (Position*) (current->value);
    if (NULL == current->next) {
        update_ledger(posArr, *h);
        return;
    }
    Position* t = (Position*) (current->next->value);

    update_tail(h, t);
    update_rope(current->next, posArr);
}

void update_tail(Position* h, Position* t) {
    if (!map_is_set()) {
        update_boundaries(h);
    }
    Position d = d_pos(*h, *t);
    move_d(t, d);
}

void update_ledger(PosArr* ledger, Position tail) {
    if (!array_contains_pos(ledger->members, tail, ledger->size)) {
        pos_arr_add(ledger, tail);
    }
}

Position* new_pos() {
    Position* p = (Position*) malloc(sizeof(Position));
    p->x = 0;
    p->y = 0;

    return p;
}

ListNode* initial_rope(size_t length) {
    if (length < 2) {
        fprintf(stderr, "Rope should be at least of length 2: %lu given\n", length);
        exit(EXIT_FAILURE);
    }

    ListNode* head = create_node(new_pos());
    ListNode* tail = head;
    for (int i = 1; i < length; i++) {
        tail = append_node(tail, new_pos());
    }

    return head;
}

void handle_loop(char c, int d, PosArr* posArr, ListNode* head) {
    Direction dir = d09_parse_direction(c);
    for (int i = 0; i < d; i++) {
        move(head->value, dir);
        update_rope(head, posArr);
    }
}

void solve_for_length(FILE* f, size_t length) {
    ListNode* rope = initial_rope(length);
    char c;
    int d;
    Position t = {0, 0};
    Position* positions = (Position*) malloc(65536*sizeof(Position));
    size_t n_ledger = 0;
    positions[n_ledger++] = t;
    PosArr posArr = {n_ledger, positions, 65536};
    char* buff;
    char next;
    while (!feof(f)) {
        c = fgetc(f);
        fgetc(f); // get rid of white space
        buff = (char*) malloc(3);
        buff[0] = fgetc(f);
        buff[1] = '\0';
        next = fgetc(f);
        if (next != '\n') {
            buff[1] = next;
            next = fgetc(f);
            buff[2] = '\0';
        }
        d = atoi(buff);
        free(buff);
        handle_loop(c, d, &posArr, rope);
    }
    free(positions);
    set_solution("%lu", posArr.size);
    free_list(rope);
}