//
// Created by Arend Hummeling on 09/12/2022.
//

#ifndef AOC2022_HELPER_H
#define AOC2022_HELPER_H

#include "../../includes/utils.h"

typedef struct Mset {
    Direction d;
    int n;
} Mset;

typedef struct PosArr {
    size_t size;
    Position* members;
    size_t allocated;
} PosArr;

Direction d09_parse_direction(char);
void update_boundaries(Position* h);

void pos_arr_add(PosArr*, Position);
void update_tail(Position*, Position*);
void update_ledger(PosArr*, Position);
ListNode* initial_rope(size_t);
void solve_for_length(FILE*, size_t);

int** d09_get_map();
bool map_is_set();
void set_map();
#endif //AOC2022_HELPER_H
