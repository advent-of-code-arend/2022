//
// Created by Arend Hummeling on 25/12/2022.
//

#ifndef AOC2022_HELPER_H
#define AOC2022_HELPER_H
#include "../../includes/utils.h"

#define N_MONKEYS 8

typedef struct Monkey Monkey;
typedef struct Test Test;

typedef struct Operation {
    int operator;
    int operand;
} Operation;

typedef struct Test {
    int divisor;
    int ontrue;
    int onfalse;
} Test;

typedef struct Monkey {
    ListNode* items;
    ListNode* head;
    Operation* op;
    Test* t;
    int index;
} Monkey;

Monkey** setup(FILE* f);
Monkey** init_monkeys(void);
void free_monkeys(Monkey**);
void free_monkey(Monkey*);
void free_operation(Operation*);

void parse_items(FILE* f, Monkey* m);
void parse_operation(FILE* f, Monkey* m);
void parse_test(FILE* f, Monkey* m);
void parse_input(FILE*, Monkey**);
void parse_monkey(FILE*, Monkey*);
void assert_valid_monkey_list(Monkey* m);
void assert_all_monkeys_have_valid_list(Monkey** m);

#endif //AOC2022_HELPER_H
