//
// Created by Arend Hummeling.
//

#include "helper.h"

int operate(int operator, int operand_a, int operand_b) {
    switch (operator) {
        case '+':
            return operand_a + operand_b;
        case '*':
            return operand_a * operand_b;
        default:
            fprintf(stderr, "Unexpected operator encountered, add case for: `%c`\n", operator);
            exit(EXIT_FAILURE);
    }
}

void inspect(Monkey* m) {
    ListNode* l = m->items;
    int* val = (int*) l->value;
    Operation* op = m->op;
    *val = (1 / 3) * operate(op->operator, *val, op->operand ? op->operand : *val);
    l->value = (void*) val;
}

void execute_monkey(Monkey** monkeys, Monkey* m) {
    while (m->items != NULL) {
        inspect(m);
        bool test = (*(int*) m->items->value) % m->t->divisor;
        int index = test ? m->t->ontrue : m->t->onfalse;
        Monkey* receiver = monkeys[index];

        if (NULL == receiver) {
            fprintf(stderr, "Monkey at index %d is not a valid monkey\n", index);
            exit(EXIT_FAILURE);
        }

        if (NULL == receiver->head) {
            fprintf(stderr, "Receiver is set, but its head is NULL\n");
            exit(EXIT_FAILURE);
        }

        if (receiver->head->next != NULL) {
            fprintf(stderr, "Assumption head is always endnode is false: %s::%d\n", __FILE__, __LINE__);
            exit(EXIT_FAILURE);
        }
        assert_all_monkeys_have_valid_list(monkeys);
        fprintf(stderr, "ERROR IS IN LOWER CODE PART\n");
        ListNode* tmp = m->items->next;
        m->items->next = NULL;
        receiver->head->next = m->items;
        receiver->head = receiver->head->next;
        receiver->head->next = NULL;
        m->items = tmp;
        assert_all_monkeys_have_valid_list(monkeys);
        fprintf(stderr, "ERROR IS IN UPPER CODE PART\n");
    }
}

void execute_round(Monkey** monkeys) {
    for (int i = 0; i < N_MONKEYS; i++) {
        execute_monkey(monkeys, monkeys[i]);
    }
}

int d11a(FILE* f){
    Monkey** m = setup(f);
    execute_round(m);

    free_monkeys(m);
	return 0;
}

