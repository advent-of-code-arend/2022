//
// Created by Arend Hummeling on 25/12/2022.
//

#include "helper.h"

void assert_all_monkeys_have_valid_list(Monkey** m) {
    for (int i = 0; i < N_MONKEYS; i++) {
        assert_valid_monkey_list(m[i]);
    }
}

void assert_valid_monkey_list(Monkey* m) {
//    fprintf(stderr, "Validating list of monkey: %d\n", m->index);
    if (NULL == m->head) {
        fprintf(stderr, "Monkey->head is NULL\n");
        exit(EXIT_FAILURE);
    }
    if (NULL != m->head->next) {
        fprintf(stderr, "Monkey->head->next is not null, ergo monkey->head is not head of list\n");
        exit(EXIT_FAILURE);
    }
}

Monkey** setup(FILE* f) {
    Monkey** m = init_monkeys();
    parse_input(f, m);
    assert_all_monkeys_have_valid_list(m);

    return m;
}

Monkey** init_monkeys() {
    Monkey** monkeys = (Monkey**) malloc(N_MONKEYS * sizeof(Monkey*));
    for (int i = 0; i < N_MONKEYS; i++) {
        Monkey* m = (Monkey*) malloc(sizeof(Monkey));
        m->index = i;
        m->t = (Test*) malloc(sizeof(Test));
        m->t->onfalse = -25;
        m->t->ontrue = -25;
        m->op = (Operation*) malloc(sizeof(Operation));
        m->items = NULL;
        m->head = NULL;
        monkeys[i] = m;
    }

    return monkeys;
}

void free_monkeys(Monkey** monkeys) {
    for (int i = 0; i < N_MONKEYS; i++) {
        free_monkey(monkeys[i]);
    }

    free(monkeys);
}

void free_monkey(Monkey* m) {
    if (m->items == NULL) {
        fprintf(stderr, "Monkey: %d has no items\n", m->index);
        exit(EXIT_FAILURE);
    }
    free_list(m->items);
    free_operation(m->op);
    free(m->t);
    free(m);
}

void free_operation(Operation* o) {
    free(o);
}

void parse_input(FILE* f, Monkey** monkeys) {
    for (int i = 0; i < N_MONKEYS; i++) {
        parse_monkey(f, monkeys[i]);
        fvassert_word(f, "\n"); // Read newline
    }
}

void parse_items(FILE* f, Monkey* m) {
    fvassert_word(f, "  Starting items: ");
    while (flookahead(f) != '\n') {
        int next = fgetc(f);
        if (next == EOF) {
            break;
        }
        if (next != ' ') {
            ungetc(next, f);
        }
        int* v = (int*) malloc(sizeof(int));
        *v = fgetn(f, ',');
        ListNode* new_item = append_node(m->head, v);
        m->head = new_item;
        assert(m->head->next == NULL);
        if (m->items == NULL) {
            m->items = new_item;
        }
    }
    fassert_char(f, '\n');
}

void parse_operation(FILE* f, Monkey* m) {
    fvassert_word(f, "  Operation: new = old ");
    fskipif(f, ' ');
    int operator = fgetc(f);
    m->op->operator = operator;
    fassert_char(f, ' ');
    if (isnumber(flookahead(f))) {
        m->op->operand = fgetn(f, '\n');
    } else {
        m->op->operand = 0;
        fvassert_word(f, "old");
        fskipif(f, 'd');
    }
//    fprintf(stderr, "Operation: new = old %c %d\n", m->op->operator, m->op->operand);

    fskipif(f, '\n');
}

void parse_test(FILE* f, Monkey* m) {
    fvassert_word(f, "  Test: divisible by ");
    fskipif(f, ' ');
    m->t->divisor = fgetn(f, '\n');
    fskipif(f, '\n');
    fvassert_word(f, "    If true: throw to monkey ");
    fskipif(f, ' ');
    m->t->ontrue = fgetc(f);
    if (!isnumber(m->t->ontrue)) {
        fprintf(stderr, "Expected number, got: (%c|%d) at %s::%d\n", m->t->ontrue, m->t->ontrue, __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    fskipif(f, '\n');
    fvassert_word(f, "    If false: throw to monkey ");
    fskipif(f, ' ');
    m->t->onfalse = fgetc(f);
    if (!isnumber(m->t->onfalse)) {
        fprintf(stderr, "Expected number, got: (%c|%d) at %s::%d\n", m->t->onfalse, m->t->onfalse, __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    fskipif(f, '\n');

    m->t->ontrue -= 48;
    m->t->onfalse -= 48;
    if (m->t->ontrue < 0 || m->t->ontrue > 10) {
        fprintf(stderr, "Monkey: %d has non-sensical ontrue value: %d\n", m->index, m->t->ontrue);
        exit(EXIT_FAILURE);
    }

    if (m->t->onfalse < 0 || m->t->onfalse > 10) {
        fprintf(stderr, "Monkey: %d has non-sensical onfalse value: %d\n", m->index, m->t->onfalse);
        exit(EXIT_FAILURE);
    }
}

void parse_monkey(FILE* f, Monkey* m) {
    fvassert_word(f, "Monkey %d:\n", m->index);
    parse_items(f, m);
    parse_operation(f, m);
    parse_test(f, m);
    fskipif(f, '\n');
}