//
// Created by Arend Hummeling.
//

#include "../../includes/utils.h"
#include "helper.h"


int d03a(FILE* f){
    int sum = 0;
    int lineno = 0;
    while (!feof(f)) {
        char buff[512];
        char first[257];
        char second[257];
        lineno++;
        fscanf(f, "%s", buff);
        size_t buff_len = strlen(buff);
        size_t half_buff_len = buff_len / 2;
        strncpy(first, buff, half_buff_len);
        first[half_buff_len] = '\0';
        strncpy(second, buff + half_buff_len, half_buff_len);
        second[half_buff_len] = '\0';

        for (int i = 0; i < half_buff_len; i++) {
            if (char_unique_in_string(first, first[i]) && char_in_string(second, first[i])) {
                int addition = map_character(first[i]);
                sum += addition;
                break;
            }
            if (char_unique_in_string(second, second[i]) && char_in_string(first, second[i])) {
                int addition = map_character(second[i]);
                sum += addition;
                break;
            }
        }
    }

    set_solution("%d", sum);
    return 0;
}

