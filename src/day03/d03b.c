//
// Created by Arend Hummeling.
//

#include "helper.h"


int d03b(FILE* f){
    int sum = 0;
    int lineno = 0;
    while (!feof(f)) {
        lineno+=3;
        char first[257];
        char second[257];
        char third[257];
        fscanf(f, "%s", first);
        fscanf(f, "%s", second);
        fscanf(f, "%s", third);
        size_t s1 = strlen(first);
        size_t s2 = strlen(second);
        size_t s3 = strlen(third);

        for (int i = 0; i < 257; i++) {
            char c = -1;
            if (i < s1) {
                c = first[i];
            }
            if (c == -1 && i < s2) {
                c = second[i];
            }
            if (c == -1 && i < s3) {
                c = third[i];
            }
            if (char_in_string(second, c) && char_in_string(third, c)) {
                sum += map_character(c);
                break;
            }
        }
    }

    set_solution("%d", sum);
    return 0;
}

