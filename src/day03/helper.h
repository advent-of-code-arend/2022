//
// Created by Arend Hummeling on 03/12/2022.
//

#ifndef AOC2022_HELPER_H
#define AOC2022_HELPER_H
#include "../../includes/utils.h"

bool char_unique_in_string(char*, char);
bool char_in_string(char*, char);
int map_character(char);

#endif //AOC2022_HELPER_H
