//
// Created by Arend Hummeling on 03/12/2022.
//

#include "helper.h"

bool char_unique_in_string(char *str, char c) {
    char *f = strchr(str, c);
    char *s = strrchr(str, c);

    return *f == *s;
}

bool char_in_string(char *str, char c) {
    char *f = strchr(str, c);

    return ((char*) NULL) != f;
}

int map_character(char a) {
    if (!isalpha(a)) {
        fprintf(stderr, "Invalid character given: %c, int: %d\n", a, a);
        return -1;
    }
    int ans = -2;
    if (isupper(a)) {
        ans = a - '@' + 26;
    }

    if (islower(a)) {
        ans = a - '`';
    }
    if (ans == -2) {
        fprintf(stderr, "Character %c is neither upper, nor lower, int: %d\n", a, a);
    }
    return ans;
}


