//
// Created by arend on 12/2/22.
//

#ifndef AOC2022_HELPER_H
#define AOC2022_HELPER_H

#include "../../includes/utils.h"

static const int n_lines = 2500;
static const int lose = 0;
static const int draw = 3;
static const int win = 6;
static const int score_rock = 1;
static const int score_paper = 2;
static const int score_scissors = 3;

typedef enum Move {
    ROCK,
    PAPER,
    SCISSORS,
} Move;

Move enemy_to_move(char);
Move my_to_move(char);
const char* move_to_string(Move);
int calculate_my_score(Move, Move);
#endif //AOC2022_HELPER_H
