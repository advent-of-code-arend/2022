//
// Created by Arend Hummeling.
//

#include "../../includes/utils.h"

#include "helper.h"


int d02a(FILE* f){
    int score = 0;
    char enemy_move, my_move;
    Move e, m;
    for (int i = 0; i < n_lines; i++) {
        fscanf(f, "%c %c ", &enemy_move, &my_move);
        e = enemy_to_move(enemy_move);
        m = my_to_move(my_move);
        score += calculate_my_score(e, m);
    }

    set_solution("%d", score);
    return 0;
}

