//
// Created by arend on 12/2/22.
//
#include "helper.h"



Move enemy_to_move(char enemy_move) {
    switch (enemy_move) {
        case 'A':
            return ROCK;
        case 'B':
            return PAPER;
        case 'C':
            return SCISSORS;
        default:
            fprintf(stderr, "Invalid enemy move: %c\n", enemy_move);
            exit(EXIT_FAILURE);
    }
}

Move my_to_move(char my_move) {
    switch (my_move) {
        case 'X':
            return ROCK;
        case 'Y':
            return PAPER;
        case 'Z':
            return SCISSORS;
        default:
            fprintf(stderr, "Invalid my move: %c\n", my_move);
            exit(EXIT_FAILURE);
    }
}

const char* move_to_string(Move move) {
    switch (move) {
        case ROCK:
            return "ROCK";
        case PAPER:
            return "PAPER";
        case SCISSORS:
            return "SCISSORS";
    }
}

int calculate_my_score(Move enemy_move, Move my_move) {
    switch (enemy_move) {
        case ROCK:
            switch (my_move) {
                case ROCK:
                    return draw + score_rock;
                case PAPER:
                    return win + score_paper;
                case SCISSORS:
                    return lose + score_scissors;
            }
        case PAPER:
            switch (my_move) {
                case ROCK:
                    return lose + score_rock;
                case PAPER:
                    return draw + score_paper;
                case SCISSORS:
                    return win + score_scissors;
            }
        case SCISSORS:
            switch (my_move) {
                case ROCK:
                    return win + score_rock;
                case PAPER:
                    return lose + score_paper;
                case SCISSORS:
                    return draw + score_scissors;
            }
    }
}