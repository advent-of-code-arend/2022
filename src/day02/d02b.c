//
// Created by Arend Hummeling.
//

#include "../../includes/utils.h"
#include "helper.h"

Move transpose_my_move(Move enemy_move, Move my_move) {
    switch (enemy_move) {
        case ROCK:
            switch (my_move) {
                case ROCK: // Lose
                    return SCISSORS;
                case PAPER: // Draw
                    return ROCK;
                case SCISSORS: // Win
                    return PAPER;
            }
        case PAPER:
            return my_move;
        case SCISSORS:
            switch (my_move) {
                case ROCK: // Lose
                    return PAPER;
                case PAPER: // Draw
                    return SCISSORS;
                case SCISSORS: // Win
                    return ROCK;

            }
    }
}


int d02b(FILE* f){
    int score = 0;
    char enemy_move, my_move;
    Move e, m;
    for (int i = 0; i < n_lines; i++) {
        fscanf(f, "%c %c ", &enemy_move, &my_move);
        e = enemy_to_move(enemy_move);
        m = my_to_move(my_move);
        m = transpose_my_move(e, m);
        score += calculate_my_score(e, m);
    }

    set_solution("%d", score);
    return 0;
}
