//
// Created by Arend Hummeling.
//

#include "../../includes/utils.h"


int d01a(FILE* f){
    char buff[6];
    int max = 0, elf = 0, c, buffcnt = 0;
    while ((c = fgetc(f)) != EOF) {
        if ('\n' == c) {
            buff[buffcnt] = '\0';
            if (buffcnt == 0) {
                if (elf > max) {
                    max = elf;
                }
                elf = 0;
                continue;
            }

            elf += atoi(buff);
            buffcnt = 0;
            continue;
        }

        buff[buffcnt++] = c;
    }
    set_solution("%d", max);

    return 0;
}

