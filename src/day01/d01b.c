//
// Created by Arend Hummeling.
//

#include "../../includes/utils.h"
typedef struct Max Max;

typedef struct Max {
    int a;
    int b;
    int c;
    int (*sum)(Max);
} Max;

int max_sum(Max max) {
    return max.a + max.b + max.c;
}

void update_max(Max* max, int elf) {
    if (elf > max->a) {
        max->c = max->b;
        max->b = max->a;
        max->a = elf;
        return;
    }
    if (elf > max->b) {
        max->c = max->b;
        max->b = elf;
        return;
    }
    if (elf > max->c) {
        max->c = elf;
    }
}


int d01b(FILE* f){
    char buff[6];
    Max max = {0, -1, -2, max_sum};
    int elf = 0, c, buffcnt = 0;
    while ((c = fgetc(f)) != EOF) {
        if ('\n' == c) {
            buff[buffcnt] = '\0';
            if (buffcnt == 0) {
                update_max(&max, elf);
                elf = 0;
                continue;
            }

            elf += atoi(buff);
            buffcnt = 0;
            continue;
        }

        buff[buffcnt++] = c;
    }

    set_solution("%d", max.sum(max));
    return 0;
}

