//
// Created by arend on 12/14/22.
//

#ifndef AOC2022_HELPER_H
#define AOC2022_HELPER_H

#include "../../includes/utils.h"

typedef struct CPU {
    size_t cycle;
    int reg;
    int strength;
    bool draw;
    FILE* output;
} CPU;

void cpu_run(FILE*, bool);
void cpu_draw(CPU* cpu);
void cpu_cycle(CPU*);
void cpu_noop(CPU*);
void cpu_add(CPU*, int);
void cpu_open_terminal(CPU* cpu);

#endif //AOC2022_HELPER_H
