//
// Created by arend on 12/14/22.
//

#include "helper.h"

void cpu_run(FILE* f, bool draw) {
    CPU* cpu = (CPU*) malloc(sizeof(CPU));
    cpu->strength = 0;
    cpu->reg = 1;
    cpu->cycle = 0;
    cpu->draw = draw;
    cpu_open_terminal(cpu);
    int c, next;
    int sign;
    char* buff;
    size_t lineno = 1;
    while (!feof(f)) {
        c = fgetc(f);
        if (c == EOF) break;
        if ('n' == c) {
            cpu_noop(cpu);
            nextline(f);
            lineno++;
            continue;
        }
        buff = (char*) malloc(5);
        while ((c = fgetc(f)) != ' ');
        if (c == EOF) break;
        if (c == '-') {
            sign = -1;
            buff[0] = fgetc(f);
        } else {
            sign = 1;
            buff[0] = c;
        }
        buff[1] = '\0';
        next = fgetc(f);
        if (next != '\n') {
            buff[1] = next;
            buff[2] = '\0';
            next = fgetc(f);
        }
        if (next != '\n') {
            buff[2] = next;
            buff[3] = '\0';
            next = fgetc(f);
        }
        if (next != '\n') {
            buff[3] = next;
            buff[4] = '\0';
            next = fgetc(f);
        }
        if ('\n' != next) {
            fprintf(stderr, "Number larger than expected at line: %lu, got: %d\n", lineno, next);
            exit(EXIT_FAILURE);
        }
        cpu_add(cpu, sign * atoi(buff));
        lineno++;
        free(buff);
    }

    if (!cpu->draw) {
        set_solution("%d", cpu->strength);
    } else {
        // figure something out
    }
    fclose(cpu->output);
    free(cpu);
}

void cpu_cycle(CPU* cpu) {
    cpu->cycle++;
    if (cpu->draw) {
        cpu_draw(cpu);
    }
    if ((cpu->cycle + 20) % 40 == 0) {
        cpu->strength += cpu->cycle * cpu->reg;
    }
}

void cpu_noop(CPU* cpu) {
    cpu_cycle(cpu);
}

void cpu_add(CPU* cpu, int i) {
    cpu_cycle(cpu);
    cpu_cycle(cpu);
    cpu->reg += i;
}

void print_sprite_position(CPU* cpu) {
    int x = (int) cpu->reg % 40;
    printf("Spite position:\t");
    for (int i = 0; i < 40; i++) {
        if (i == (x - 1) || i == x | i == (x + 1)) {
            putchar('#');
        } else {
            putchar('.');
        }
    }
    putchar('\n');
}

bool is_on_sprite(CPU* cpu) {
    return abs((int)cpu->reg - ((int)cpu->cycle % 40)) > 1;
}

void cpu_draw(CPU* cpu) {
    print_sprite_position(cpu);
    fprintf(cpu->output, "%s", is_on_sprite(cpu) ? " " : "\u2588");
    if (!(cpu->cycle % 40)) {
        fputc('\n', cpu->output);
    }
}

void cpu_open_terminal(CPU* cpu) {
    cpu->output = fopen(cpu->draw ? "output.txt" : "/dev/null", "w");
    setbuf(cpu->output, NULL);
}