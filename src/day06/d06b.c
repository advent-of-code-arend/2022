//
// Created by Arend Hummeling.
//

#include "../../includes/utils.h"

int d06b(FILE* f){
    set_solution("%d", first_unique_string(f, 14));
    return 0;
}

