//
// Created by Arend Hummeling.
//

#include "../../includes/utils.h"

int d06a(FILE *f) {
    set_solution("%d", first_unique_string(f, 4));
    return 0;
}

