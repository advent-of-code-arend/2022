//
// Created by Arend Hummeling.
//

#include "../../includes/utils.h"


int d04b(FILE* f){
    int a, b, c, d, lineno = 0, count = 0;
    while (!feof(f)) {
        lineno++;
        fscanf(f, "%d-%d,%d-%d", &a, &b, &c, &d);
        count += (int) range_overlaps_with_range((Range) {a, b}, (Range) {c, d});
    }

    set_solution("%d", count);
    return 0;
}

