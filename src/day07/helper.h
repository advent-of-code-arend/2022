//
// Created by Arend Hummeling on 08/12/2022.
//

#ifndef AOC2022_HELPER_H
#define AOC2022_HELPER_H

#include "../../includes/utils.h"

#define MAX_COUNT_SIZE 100000

typedef struct Dir Dir;
typedef struct File File;

typedef struct File {
    char* filename;
    size_t size;
    Dir* dir;
} File;

typedef struct Dir {
    char* path;
    char* name;
    size_t pathlen;
    size_t size;
    Dir* parent;
    Dir** subdirs;
    char** refs;
    size_t n_subdirs;
    File* files;
    size_t n_files;
} Dir;

Dir* create_filesystem(FILE*);
void add_dir(Dir*, Dir*);
void validate_dir(Dir*);
Dir* create_dir(Dir*, char*);
size_t get_dir_size_recursively(Dir*);
void validate_dir(Dir*);
void change_directory(Dir**, char*);
void add_file(Dir*, char*, size_t);
void free_dir(Dir*);
Dir* create_filesystem(FILE*);
void add_dir(Dir*, Dir*);
#endif //AOC2022_HELPER_H
