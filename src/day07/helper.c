//
// Created by Arend Hummeling on 08/12/2022.
//

#include "helper.h"

void add_dir(Dir* parent, Dir* child) {
    parent->subdirs[parent->n_subdirs] = child;
    parent->refs[parent->n_subdirs] = child->name;
    parent->n_subdirs++;
}

Dir* create_dir(Dir* parent, char* name) {
    Dir* dir = (Dir*) malloc(sizeof(Dir));
    char* path = name;
    dir->name = name;
    dir->parent = dir;
    if (NULL != parent) {
        if (parent->pathlen + strlen(name) > BUFF_SIZE - 16) {
            fprintf(stderr, "Path is coming too long!\n");
            exit(EXIT_FAILURE);
        }
        char* pathbuff = calloc(BUFF_SIZE, sizeof(char));
        strncat(pathbuff, parent->path, parent->pathlen);
        if ('/' != parent->path[parent->pathlen - 1]) {
            strncat(pathbuff, "/", 2);
        }
        strncat(pathbuff, name, strlen(name));
        path = strdup(pathbuff);
        free(pathbuff);
        dir->parent = parent;
        add_dir(parent, dir);
    }
    dir->path = path;
    dir->pathlen = strlen(path);
    dir->size = -1;
    dir->subdirs = (Dir**) malloc(BUFF_SIZE*sizeof(Dir*));
    dir->refs = (char**) malloc(BUFF_SIZE*sizeof(char*));
    dir->n_subdirs = 0;
    dir->files = (File*) malloc(BUFF_SIZE*sizeof(File*));
    dir->n_files = 0;
    validate_dir(dir);

    return dir;
}

size_t get_dir_size_recursively(Dir* dir) {
    if (-1 != dir->size) {
        return dir->size;
    }
    dir->size = 0;
    for (int i = 0; i < dir->n_files; i++) {
        dir->size += dir->files[i].size;
    }
    for (int i = 0; i < dir->n_subdirs; i++) {
        dir->size += get_dir_size_recursively(dir->subdirs[i]);
    }

    return dir->size;
}

void validate_dir(Dir* dir) {
    if (strncmp("/", dir->path, 1) || !strncmp("//", dir->path, 2)) {
        fprintf(stderr, "Directory does not have a valid path: %s\n", dir->path);
        exit(EXIT_FAILURE);
    }
}

void change_directory(Dir** context, char* name) {
    if (NULL != strchr(name, '/')) {
        fprintf(stderr, "Target invalid: / is not allowed in target name\n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < (*context)->n_subdirs; i++) {
        if (!strcmp(name, (*context)->refs[i])) {
            *context = (*context)->subdirs[i];
            return;
        }
    }
    fprintf(stderr, "%s: No such file or directory\n", name);
    exit(EXIT_FAILURE);
}

void add_file(Dir* parent, char* name, size_t size) {
    parent->files[parent->n_files++] = (File) {
            name,
            size,
            parent,
    };
}

void free_dir(Dir* dir) {
    free(dir->files);
    for (int i = 0; i < dir->n_subdirs; i++) {
        free_dir(dir->subdirs[i]);
    }
    free(dir->subdirs);
}

Dir* create_filesystem(FILE* f) {
    nextline(f);
    Dir* context = create_dir(NULL, "/");
    Dir* root = context;
    while (!feof(f)) {
        validate_dir(context);
        char buff[1024], c;
        int p = 0;
        while ((c = fgetc(f)) != '\n' && !feof(f)) {
            buff[p++] = c;
        }
        buff[p] = '\0';
        if (buff[0] == '$') {
            // ls
            if (buff[2] == 'l') {
                continue;
            }
            if (buff[2] == 'c') {
                // cd ..
                if (buff[5] == '.') {
                    context = context->parent;
                    continue;
                }
                // cd some_dir
                char* new_dir = calloc(1024, sizeof(char));
                for (int i = 5; i < p; i++) {
                    new_dir[i - 5] = (char) buff[i];
                    if ('/' == buff[i]) {
                        fprintf(stderr, "New dirname should not contain /, buff: %s\n", buff);
                        exit(EXIT_FAILURE);
                    }
                }
                new_dir[p] = '\0';
                char* dup = strdup(new_dir);
                change_directory(&context, dup);
                free(new_dir);
                continue;
            }
            fprintf(stderr, "Unexpected line: %s\n", buff);
            exit(EXIT_FAILURE);
        }
        char* namebuff = malloc(128);
        // ls result: dir
        if (buff[0] == 'd') {
            for (int i = 4; i < p; i++ ){
                namebuff[i - 4] = buff[i];
            }
            namebuff[p] = '\0';
            char* dup = strdup(namebuff);
            create_dir(context, dup);
            free(namebuff);
            continue;
        }
        // ls result: file
        char intbuff[64];
        int index = 0;
        while (buff[index] != ' ') {
            intbuff[index] = (char) buff[index];
            index++;
        }
        intbuff[index] = '\0';
        for (int i = index; i < p; i++) {
            namebuff[i - index] = (char) buff[i];
        }
        namebuff[p] = '\0';
        size_t s = (size_t) strtol(intbuff, (char **)NULL, 10);
        if (s == 0) {
            fprintf(stderr, "File size is 0, intbuff: %s\t buff: %s\n", intbuff, buff);
            exit(EXIT_FAILURE);
        }
        char* namedup = strdup(namebuff);
        free(namebuff);
        add_file(context, namedup, s);
    }

    return root;
}