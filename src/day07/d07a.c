//
// Created by Arend Hummeling.
//

#include "helper.h"

void count_sum(Dir* dir, int* sum) {
    if (-1 == dir->size) {
        fprintf(stderr, "Dirs are not yet sized\n");
        exit(EXIT_FAILURE);
    }
    if (dir->size < MAX_COUNT_SIZE) {
        (*sum) += dir->size;

//        return;
    }

    for (int i = 0; i < dir->n_subdirs; i++) {
        count_sum(dir->subdirs[i], sum);
    }
}

int d07a(FILE* f){
    int sum = 0;
    Dir* root = create_filesystem(f);
    get_dir_size_recursively(root);
    count_sum(root, &sum);

    int lower_bound = 1254298;

    if (sum <= lower_bound) {
        fprintf(stderr, "%d: too low\n", sum);
        exit(EXIT_FAILURE);
    }

    free_dir(root);
    set_solution("%d", sum);
	return sum;
}

