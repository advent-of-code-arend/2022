//
// Created by Arend Hummeling.
//

#include "helper.h"

#define TOTAL_SPACE 70000000
#define REQUIRED 30000000

void find_smallest_directory(Dir* dir, int to_delete, size_t* size) {
    if (dir->size < to_delete) {
        return;
    }
    if (dir->size < *size) {
        *size = dir->size;
    }
    for (int i = 0; i < dir->n_subdirs; i++) {
        find_smallest_directory(dir->subdirs[i], to_delete, size);
    }
}

int d07b(FILE* f){
    Dir* root = create_filesystem(f);
    size_t size = root->size;
    find_smallest_directory(root, get_dir_size_recursively(root) - (TOTAL_SPACE - REQUIRED), &size);

    free_dir(root);
    set_solution("%lu", size);
    return size;
}

