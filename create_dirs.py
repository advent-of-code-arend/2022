import os
import sys

_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'src')
force = '--force' in sys.argv


def create_c_file(dirname, dstr, day_dir):
    fname = os.path.join(dirname, f'{dstr}.c')
    print(os.path.join('src', day_dir, f'{dstr}.c'), end=' ')
    if os.path.isfile(fname) and not force:
        return
    with open(fname, 'w') as file:
        print('//', file=file)
        print('// Created by Arend Hummeling.', file=file)
        print('//', file=file)
        print('', file=file)
        print('#include "../../includes/utils.h"', file=file)
        print('', file=file)
        print('', file=file)
        print(f'int {dstr}(FILE* f)', '{', sep='', file=file)
        print('', file=file)
        print('\treturn 0;', file=file)
        print('}', file=file)
        print('', file=file)


def create_dir(day):
    day_name = f'day{day:02d}'
    dirname = os.path.join(_DIR, day_name)
    if not os.path.isdir(dirname):
        os.mkdir(dirname)
    gitkeepfile = os.path.join(dirname, '.gitkeep')
    dstra = f'd{day:02d}a'
    dstrb = f'd{day:02d}b'
    if not os.path.isfile(gitkeepfile):
        os.popen(f'touch {gitkeepfile}')
    create_c_file(dirname, dstra, day_name)
    create_c_file(dirname, dstrb, day_name)


[create_dir(x) for x in range(1, 26)]
