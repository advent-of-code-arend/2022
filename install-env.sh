#!/bin/bash
set -euo pipefail

if [ ! -f /etc/os-release ]; then
  echo "This only runs on Linux"
  exit 2
fi

DEBIAN_FRONTEND=noninteractive

for i in git automake curl autopoint; do
  if ! command -v "$i"; then
    apt-get -yqq update && apt-get -yqq install git automake binutils curl autopoint libc6-dbg
  fi
done

function usage() {
  echo "install-env -e cmake|valgrind|ninja|gdb|bison"
}

while getopts ":he:" arg; do
  case $arg in
    e)
      S="/scripts/${OPTARG}.sh"
      if [ ! -f "${S}" ]; then
        echo "${S}: No such file or directory"
        exit 1
      fi
      bash "${S}"
      ;;
    h | *) # Display help.
      usage
      exit 0
      ;;
  esac
done
